podkatcher
==========

This command-line application written in Kotlin parses podcast feeds
and downloads audio files according to a specified format.

```
Usage: podkatcher.jar [OPTION...] url

Available options:

    --help                      displays this help text and exits the program
    --dry-run                   only show what would happen, do not download
    --format=FORMAT             specifies the file name format, may contain sub directories
                                default is: "%{date} - %{title}"

                                available format options:
                                %{date}, %{day}, %{month}, %{year}, %{title}
    --output-directory=DIR      top-level directory for downloading the files to
    --since=DATE                only download files published after the specified date in ISO format
    --write-config              writes a modifiable config file to '$HOME/.podcast-downloader.yaml'
                                and exists. the config values are based on the current arguments.
```