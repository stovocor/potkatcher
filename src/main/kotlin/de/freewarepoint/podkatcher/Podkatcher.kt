package de.freewarepoint.podkatcher

import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication

@SpringBootApplication
class Podkatcher

fun main(args: Array<String>) {
    runApplication<Podkatcher>(*args)
}