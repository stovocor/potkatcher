package de.freewarepoint.podkatcher.options

import com.amihaiemil.eoyaml.YamlMapping
import com.amihaiemil.eoyaml.YamlMappingBuilder

interface PersistableOption<T> : CommandLineOption<T> {
    fun setFromYamlMapping(yamlMapping: YamlMapping)
    fun addToYamlConfig(yamlMappingBuilder: YamlMappingBuilder): YamlMappingBuilder {
        return yamlMappingBuilder
    }
}