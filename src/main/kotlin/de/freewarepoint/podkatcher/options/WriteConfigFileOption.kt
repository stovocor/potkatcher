package de.freewarepoint.podkatcher.options

import org.springframework.boot.ApplicationArguments

class WriteConfigFileOption : CommandLineOption<Boolean> {
    override var value = false
        private set

    override fun setFromArguments(arguments: ApplicationArguments) {
        value = arguments.optionNames.contains("write-config")
    }
}