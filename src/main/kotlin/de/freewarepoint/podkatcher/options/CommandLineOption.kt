package de.freewarepoint.podkatcher.options

import org.springframework.boot.ApplicationArguments

interface CommandLineOption<T> {
    val value: T?
    fun setFromArguments(arguments: ApplicationArguments)
}