package de.freewarepoint.podkatcher.options

import de.freewarepoint.podkatcher.ConfigReader
import org.springframework.boot.ApplicationArguments
import java.nio.file.Files

class HelpOption : CommandLineOption<Boolean> {
    override var value = false
        private set

    override fun setFromArguments(arguments: ApplicationArguments) {
        value = if (arguments.optionNames.contains(OPTION_NAME)) {
            true
        } else {
            arguments.nonOptionArgs.isEmpty() && !Files.exists(ConfigReader.CONFIG_FILE)
        }
    }

    companion object {
        private const val OPTION_NAME = "help"
    }
}