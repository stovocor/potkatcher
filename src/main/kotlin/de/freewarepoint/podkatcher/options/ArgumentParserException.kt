package de.freewarepoint.podkatcher.options

class ArgumentParserException internal constructor(message: String?) : RuntimeException(message)