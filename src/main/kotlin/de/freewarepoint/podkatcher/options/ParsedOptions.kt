package de.freewarepoint.podkatcher.options

import com.amihaiemil.eoyaml.YamlMapping
import com.amihaiemil.eoyaml.YamlMappingBuilder
import org.springframework.boot.ApplicationArguments
import java.net.URL
import java.nio.file.Path
import java.time.ZoneId
import java.time.ZonedDateTime
import java.util.function.Consumer

open class ParsedOptions {
    private val url = UrlOption()
    private val format = FormatOption()
    private val sinceDate = SinceDateOption()
    private val dryRun = DryRunOption()
    private val readConfigFile = ReadConfigOption()
    private val writeConfigFile = WriteConfigFileOption()
    private val help = HelpOption()
    private val outputDirectory = OutputDirectoryOption()
    private val allOptions = listOf(
        help, url, format, sinceDate, dryRun,
        readConfigFile, writeConfigFile, outputDirectory
    )

    constructor(arguments: ApplicationArguments) {
        help.setFromArguments(arguments)
        if (!help.value) {
            allOptions.forEach(Consumer { option: CommandLineOption<*> -> option.setFromArguments(arguments) })
        }
    }

    constructor(arguments: ApplicationArguments, yamlMapping: YamlMapping) {
        help.setFromArguments(arguments)

        if (!help.value) {
            allOptions.forEach(Consumer { option: CommandLineOption<*> -> option.setFromArguments(arguments) })
            allOptions.stream().filter { o: CommandLineOption<*>? -> PersistableOption::class.java.isInstance(o) }
                    .map { obj: CommandLineOption<*>? -> PersistableOption::class.java.cast(obj) }
                    .forEach { o: PersistableOption<*> -> o.setFromYamlMapping(yamlMapping) }
        }
    }

    fun applyToYamlConfig(yamlMappingBuilder: YamlMappingBuilder): YamlMappingBuilder {
        var builderToReturn: YamlMappingBuilder = yamlMappingBuilder
        for (option in allOptions) {
            if (option is PersistableOption<*>) {
                builderToReturn = option.addToYamlConfig(builderToReturn)
            }
        }
        return builderToReturn
    }

    fun getUrl(): URL {
        return url.value
    }

    fun getFormat(): String {
        return format.value
    }

    fun getSinceDate(): ZonedDateTime {
        return sinceDate.value.atStartOfDay(ZoneId.systemDefault())
    }

    open fun isDryRun(): Boolean {
        return dryRun.value
    }

    val configFile: Path?
        get() = readConfigFile.value

    fun isWriteConfigFile(): Boolean {
        return writeConfigFile.value
    }

    fun isHelp(): Boolean {
        return help.value
    }

    fun getOutputDirectory(): Path {
        return outputDirectory.value
    }
}