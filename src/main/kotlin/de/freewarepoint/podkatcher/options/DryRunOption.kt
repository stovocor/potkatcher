package de.freewarepoint.podkatcher.options

import org.springframework.boot.ApplicationArguments

class DryRunOption : CommandLineOption<Boolean> {
    override var value = false
        private set

    override fun setFromArguments(arguments: ApplicationArguments) {
        value = arguments.optionNames.contains(OPTION_NAME)
    }

    companion object {
        private const val OPTION_NAME = "dry-run"
    }
}