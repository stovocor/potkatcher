package de.freewarepoint.podkatcher.options

import com.amihaiemil.eoyaml.YamlMapping
import com.amihaiemil.eoyaml.YamlMappingBuilder
import org.springframework.boot.ApplicationArguments
import java.net.MalformedURLException
import java.net.URL
import java.nio.file.Paths

class UrlOption : PersistableOption<URL> {
    private var argumentValue: URL? = null
    private var configValue: URL? = null

    override fun setFromArguments(arguments: ApplicationArguments) {
        if (arguments.nonOptionArgs.isEmpty()) {
            argumentValue = null
            return
        }
        val url = arguments.nonOptionArgs.iterator().next()
        argumentValue = toUrl(url)
    }

    override fun addToYamlConfig(yamlMappingBuilder: YamlMappingBuilder): YamlMappingBuilder {
        return if (argumentValue != null) {
            yamlMappingBuilder.add(OPTION_NAME, value.toString())
        } else {
            yamlMappingBuilder
        }
    }

    override fun setFromYamlMapping(yamlMapping: YamlMapping) {
        configValue = toUrl(yamlMapping.string(OPTION_NAME))
    }

    override val value: URL
        get() = if (argumentValue == null && configValue == null) {
            throw ArgumentParserException("Feed URL required")
        } else {
            argumentValue ?: configValue!!
        }

    companion object {
        private const val OPTION_NAME = "url"
        private fun toUrl(url: String): URL {
            return try {
                URL(url)
            } catch (e: MalformedURLException) {
                try {
                    Paths.get(url).toUri().toURL()
                } catch (e1: MalformedURLException) {
                    throw ArgumentParserException("Invalid feed URL specified: '$url'.")
                }
            }
        }
    }
}