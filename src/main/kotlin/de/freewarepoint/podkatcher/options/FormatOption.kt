package de.freewarepoint.podkatcher.options

import com.amihaiemil.eoyaml.YamlMapping
import com.amihaiemil.eoyaml.YamlMappingBuilder
import org.springframework.boot.ApplicationArguments

class FormatOption : PersistableOption<String> {

    private var argumentValue: String? = null
    private var configValue: String? = null

    override fun setFromArguments(arguments: ApplicationArguments) {
        if (arguments.optionNames.contains(OPTION_NAME)) {
            val values = arguments.getOptionValues(OPTION_NAME)
            argumentValue = if (values.size != 1) {
                throw ArgumentParserException("Option '$OPTION_NAME' requires exactly one argument.")
            } else {
                values.iterator().next()
            }
        }
    }

    override fun setFromYamlMapping(yamlMapping: YamlMapping) {
        configValue = yamlMapping.string(OPTION_NAME)
    }

    override val value: String
        get() = argumentValue ?: configValue ?: DEFAULT

    override fun addToYamlConfig(yamlMappingBuilder: YamlMappingBuilder): YamlMappingBuilder {
        return yamlMappingBuilder.add(OPTION_NAME, value)
    }

    companion object {
        private const val OPTION_NAME = "format"
        private const val DEFAULT = "%{date} - %{title}"
    }

}