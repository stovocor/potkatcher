package de.freewarepoint.podkatcher.options

import com.amihaiemil.eoyaml.YamlMapping
import com.amihaiemil.eoyaml.YamlMappingBuilder
import org.springframework.boot.ApplicationArguments
import java.nio.file.Files
import java.nio.file.Path
import java.nio.file.Paths

class OutputDirectoryOption : PersistableOption<Path> {

    companion object {
        private const val OPTION_NAME = "output-directory"
        private val DEFAULT = Paths.get(".")
    }

    private var argumentValue: Path? = null
    private var configValue: Path? = null

    override val value: Path
        get() = argumentValue ?: configValue ?: DEFAULT

    override fun setFromArguments(arguments: ApplicationArguments) {
        if (!arguments.optionNames.contains(OPTION_NAME)) {
            return
        }
        val values = arguments.getOptionValues(OPTION_NAME)
        if (values.size != 1) {
            throw ArgumentParserException("Option 'output-directory' requires exactly one argument.")
        }
        argumentValue = checkDirectory(Paths.get(values[0]))
    }

    override fun setFromYamlMapping(yamlMapping: YamlMapping) {
        configValue = checkDirectory(Paths.get(yamlMapping.string(OPTION_NAME)))
    }

    private fun checkDirectory(directory: Path): Path {
        return if (!Files.exists(directory)) {
            throw ArgumentParserException("Output directory '" + directory.toAbsolutePath() + "' does not exist.")
        } else if (!Files.isDirectory(directory)) {
            throw ArgumentParserException("Output path '" + directory.toAbsolutePath() + "' is not a directory.")
        } else {
            directory
        }
    }

    override fun addToYamlConfig(yamlMappingBuilder: YamlMappingBuilder): YamlMappingBuilder {
        return yamlMappingBuilder.add(OPTION_NAME, value.toAbsolutePath().toString())
    }

}