package de.freewarepoint.podkatcher.options

import org.springframework.boot.ApplicationArguments
import java.nio.file.Files
import java.nio.file.Path
import java.nio.file.Paths

class ReadConfigOption : CommandLineOption<Path?> {

    companion object {
        private const val OPTION_NAME = "config"
        private val DEFAULT = Paths.get(System.getProperty("user.home")).resolve(".podcast-downloader.yaml")
    }

    private var argumentValue: Path? = null

    override val value: Path?
        get() = argumentValue ?: DEFAULT

    override fun setFromArguments(arguments: ApplicationArguments) {
        if (!arguments.optionNames.contains(OPTION_NAME)) {
            return
        }
        val values = arguments.getOptionValues(OPTION_NAME)
        val configFile: Path
        if (values.size == 0) {
            throw ArgumentParserException("Option 'config' requires exactly one argument.")
        } else {
            configFile = Paths.get(values.iterator().next()).toAbsolutePath()
            if (!Files.isRegularFile(configFile)) {
                throw ArgumentParserException("Unable to read file '$configFile'.")
            }
            argumentValue = configFile
        }
    }

}