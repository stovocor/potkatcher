package de.freewarepoint.podkatcher.options

import com.amihaiemil.eoyaml.YamlMapping
import com.amihaiemil.eoyaml.YamlMappingBuilder
import org.springframework.boot.ApplicationArguments
import java.time.LocalDate

class SinceDateOption : PersistableOption<LocalDate> {

    private var argumentValue: LocalDate? = null
    private var configValue: LocalDate? = null

    override fun setFromArguments(arguments: ApplicationArguments) {
        if (arguments.optionNames.contains(OPTION_NAME)) {
            val values = arguments.getOptionValues(OPTION_NAME)
            argumentValue = if (values.size != 1) {
                throw ArgumentParserException("Option '$OPTION_NAME' requires exactly one argument.")
            } else {
                LocalDate.parse(values.iterator().next())
            }
        }
    }

    override fun setFromYamlMapping(yamlMapping: YamlMapping) {
        val since = yamlMapping.string(OPTION_NAME)
        configValue = if (since != null) {
            LocalDate.parse(since)
        } else {
            null
        }
    }

    override fun addToYamlConfig(yamlMappingBuilder: YamlMappingBuilder): YamlMappingBuilder {
        return if (argumentValue != null) {
            yamlMappingBuilder.add(OPTION_NAME, value.toString())
        } else {
            yamlMappingBuilder
        }
    }

    override val value: LocalDate
        get() = argumentValue ?: configValue ?: DEFAULT

    companion object {
        private const val OPTION_NAME = "since"
        private val DEFAULT = LocalDate.MIN
    }

}