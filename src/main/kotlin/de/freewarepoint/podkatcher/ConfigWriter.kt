package de.freewarepoint.podkatcher

import com.amihaiemil.eoyaml.Yaml
import de.freewarepoint.podkatcher.options.ParsedOptions
import java.io.IOException
import java.io.UncheckedIOException
import java.nio.file.Files
import java.nio.file.Path

internal class ConfigWriter(private val parsedOptions: ParsedOptions) {

    private var configFile = ConfigReader.CONFIG_FILE

    fun write() {
        val yamlMappingBuilder = Yaml.createYamlMappingBuilder()
        val yaml = parsedOptions.applyToYamlConfig(yamlMappingBuilder).build()
        try {
            Files.write(configFile, listOf(yaml.toString()))
        } catch (e: IOException) {
            throw UncheckedIOException(e)
        }
    }

    fun setConfigFile(configFile: Path) {
        // For unit tests only
        this.configFile = configFile
    }
}