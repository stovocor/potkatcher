package de.freewarepoint.podkatcher

import de.freewarepoint.podkatcher.options.ArgumentParserException
import org.slf4j.LoggerFactory
import org.springframework.boot.SpringBootExceptionReporter
import org.springframework.context.ConfigurableApplicationContext

class ExceptionReporter(
        @Suppress("unused") private val context: ConfigurableApplicationContext) : SpringBootExceptionReporter {

    companion object {
        private val LOGGER = LoggerFactory.getLogger(ExceptionReporter::class.java)
    }

    override fun reportException(failure: Throwable): Boolean {
        if (failure is IllegalStateException && failure.cause is ArgumentParserException) {
            LOGGER.error((failure.cause as ArgumentParserException).message)
            return true
        }
        return false
    }

}