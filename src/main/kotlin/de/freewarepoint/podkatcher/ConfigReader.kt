package de.freewarepoint.podkatcher

import com.amihaiemil.eoyaml.Yaml
import com.amihaiemil.eoyaml.YamlMapping
import org.slf4j.LoggerFactory
import java.io.IOException
import java.io.UncheckedIOException
import java.nio.file.Files
import java.nio.file.Path
import java.nio.file.Paths

object ConfigReader {
    private val LOGGER = LoggerFactory.getLogger(ConfigReader::class.java)
    val CONFIG_FILE: Path = Paths.get(System.getProperty("user.home")).resolve(".podcast-downloader.yaml")

    fun readConfigFile(configFile: Path = CONFIG_FILE): YamlMapping? {
        if (!Files.exists(configFile)) {
            return null
        }
        if (!Files.isReadable(configFile)) {
            LOGGER.error("Config file '$configFile' is not readable.")
            return null
        }
        if (!Files.isRegularFile(configFile)) {
            LOGGER.error("Config file '$configFile' is not a file.")
            return null
        }
        val yamlMapping: YamlMapping = try {
            Yaml.createYamlInput(configFile.toFile()).readYamlMapping()
        } catch (e: IOException) {
            throw UncheckedIOException(e)
        }
        return yamlMapping
    }
}