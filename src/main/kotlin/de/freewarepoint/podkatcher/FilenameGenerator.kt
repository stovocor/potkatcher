package de.freewarepoint.podkatcher

import be.ceau.podcastparser.models.core.Item
import de.freewarepoint.podkatcher.options.ParsedOptions
import org.apache.commons.text.StringSubstitutor

internal class FilenameGenerator(private val parsedOptions: ParsedOptions) {
    fun filenameForItem(item: Item): String {
        val values: MutableMap<String, String> = HashMap()
        if (item.pubDate != null) {
            values["date"] = item.pubDate.toLocalDate().toString()
            values["day"] = String.format("%02d", item.pubDate.dayOfMonth)
            values["month"] = String.format("%02d", item.pubDate.monthValue)
            values["year"] = item.pubDate.year.toString()
        }
        values["title"] = if (item.title != null) removeIllegalCharacters(item.title.text) else "Unknown title"
        val url = item.enclosure.url
        val extension = url.substring(url.lastIndexOf("."))
        val stringSubstitutor = StringSubstitutor(values, "%{", "}")
        return stringSubstitutor.replace(parsedOptions.getFormat()) + extension
    }

    private fun removeIllegalCharacters(str: String): String {
        return str.replace(Regex("[/\\\\]"), "_")
    }
}