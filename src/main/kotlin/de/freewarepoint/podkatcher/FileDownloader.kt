package de.freewarepoint.podkatcher

import de.freewarepoint.podkatcher.options.ParsedOptions
import org.slf4j.LoggerFactory
import java.io.IOException
import java.net.URL
import java.nio.channels.Channels
import java.nio.channels.FileChannel
import java.nio.file.Files
import java.nio.file.Path
import java.nio.file.StandardOpenOption

internal class FileDownloader(private val parsedOptions: ParsedOptions, private val statistics: Statistics) {

    companion object {
        private val LOGGER = LoggerFactory.getLogger(FileDownloader::class.java)
    }

    fun download(url: URL, path: Path, expectedSize: Long) {
        if (fileExists(path, expectedSize)) {
            return
        }
        LOGGER.info("Downloading '{}'...", path)
        if (!parsedOptions.isDryRun()) {
            if (!Files.isDirectory(path.parent)) {
                try {
                    Files.createDirectories(path.parent)
                } catch (e: IOException) {
                    LOGGER.error("Unable to create '" + path.parent + "'.", e)
                    return
                }
            }
            try {
                Channels.newChannel(url.openStream()).use { `in` ->
                    FileChannel.open(path, StandardOpenOption.CREATE_NEW, StandardOpenOption.WRITE).use { out ->
                        val bytesTransferred = out.transferFrom(`in`, 0, Long.MAX_VALUE)
                        statistics.recordDownloadedFile(bytesTransferred)
                    }
                }
            } catch (e: IOException) {
                LOGGER.error("Download failed", e)
                statistics.recordFailedDownload()
            }
        } else {
            statistics.recordDownloadedFile(expectedSize)
        }
    }

    private fun fileExists(path: Path, expectedSize: Long): Boolean {
        if (!Files.exists(path)) {
            return false
        }
        LOGGER.info("Skipping '{}' because it already exists.", path)
        val existingSize = Files.size(path)
        if (existingSize != expectedSize) {
            LOGGER.warn("Existing file '{}' has unexpected size ({} != {})", path, existingSize, expectedSize)
            statistics.recordExistingFileWithDifferentSize(expectedSize)
        } else {
            statistics.recordExistingFileWithMatchingSize(expectedSize)
        }
        return true
    }

}