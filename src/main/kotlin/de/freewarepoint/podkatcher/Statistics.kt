package de.freewarepoint.podkatcher

import org.springframework.util.unit.DataSize

class Statistics {
    var totalFiles = 0

    internal var existingFilesWithMatchingSize = 0
    internal var existingFilesWithDifferentSize = 0
    internal var filesTooOld = 0
    internal var downloadedFiles = 0
    internal var failedDownloads = 0
    private var bytesTransferred: Long = 0
    private var bytesSaved: Long = 0
    private var duration: Long = 0
    fun recordExistingFileWithMatchingSize(size: Long) {
        existingFilesWithMatchingSize++
        bytesSaved += size
    }

    fun recordExistingFileWithDifferentSize(expectedSize: Long) {
        existingFilesWithDifferentSize++
        bytesSaved += expectedSize
    }

    fun recordFileTooOld() {
        filesTooOld++
    }

    fun recordDownloadedFile(bytesTransferred: Long) {
        downloadedFiles++
        this.bytesTransferred += bytesTransferred
    }

    fun recordFailedDownload() {
        failedDownloads++
    }

    val downloadSpeed: String
        get() {
            val transferredSize = DataSize.ofBytes(bytesTransferred)
            val seconds = if (duration >= 1000) duration / 1000 else 1
            val speed = DataSize.ofBytes(bytesTransferred / seconds)
            return transferredSize.toMegabytes().toString() + "MB in " + seconds + "s (" + speed.toMegabytes() + "MB/s)"
        }

    fun setDurationInMillis(duration: Long) {
        this.duration = duration
    }
}