package de.freewarepoint.podkatcher

import be.ceau.podcastparser.PodcastParser
import be.ceau.podcastparser.models.core.Feed
import de.freewarepoint.podkatcher.ConfigReader.readConfigFile
import de.freewarepoint.podkatcher.options.ParsedOptions
import org.slf4j.LoggerFactory
import org.springframework.boot.ApplicationArguments
import org.springframework.boot.ApplicationRunner
import org.springframework.stereotype.Component
import org.springframework.util.StopWatch
import java.io.BufferedInputStream
import java.io.BufferedReader
import java.io.FileNotFoundException
import java.io.InputStreamReader
import java.net.URL
import java.nio.file.Files
import java.nio.file.Paths

@Component
class Runner : ApplicationRunner {

    companion object {
        private val LOGGER = LoggerFactory.getLogger(Runner::class.java)
    }

    override fun run(args: ApplicationArguments) {
        val configFile = Paths.get(System.getProperty("user.home")).resolve(".podcast-downloader.yaml")

        val parsedOptions: ParsedOptions = if (Files.isRegularFile(configFile)) {
            readConfigFile()?.let { ParsedOptions(args, it) } ?: ParsedOptions(args)
        } else {
            ParsedOptions(args)
        }

        if (parsedOptions.isHelp()) {
            displayHelp()
            return
        }
        if (parsedOptions.isWriteConfigFile()) {
            ConfigWriter(parsedOptions).write()
            return
        }
        val url = parsedOptions.getUrl()
        var feed: Feed
        try {
            InputStreamReader(BufferedInputStream(url.openStream())).use { reader -> feed = PodcastParser().parse(reader) }
        } catch (e: FileNotFoundException) {
            LOGGER.error("Unable to read feed from '{}'.", url)
            return
        }
        val statistics = Statistics()
        val stopWatch = StopWatch()
        stopWatch.start()
        val filenameGenerator = FilenameGenerator(parsedOptions)
        val fileDownloader = FileDownloader(parsedOptions, statistics)
        val outputDirectory = parsedOptions.getOutputDirectory()
        for (item in feed.items) {
            val itemUrl = URL(item.enclosure.url)
            val path = outputDirectory.resolve(filenameGenerator.filenameForItem(item))
            if (item.pubDate.isAfter(parsedOptions.getSinceDate())) {
                fileDownloader.download(itemUrl, path, item.enclosure.length)
            } else {
                statistics.recordFileTooOld()
            }
        }
        stopWatch.stop()
        statistics.setDurationInMillis(stopWatch.totalTimeMillis)
        statistics.totalFiles = feed.items.size
        printStatistics(statistics, parsedOptions)
    }

    private fun displayHelp() {
        val inputStream = javaClass.classLoader.getResourceAsStream("help.txt")
        if (inputStream == null) {
            LOGGER.error("Unable to find help text")
            return
        }
        BufferedReader(InputStreamReader(inputStream)).use { reader ->
            var line: String?
            while (reader.readLine().also { line = it } != null) {
                LOGGER.info(line)
            }
        }
    }

    private fun printStatistics(statistics: Statistics, parsedOptions: ParsedOptions) {
        val downloadSpeed = if (parsedOptions.isDryRun()) {
            "N/A"
        } else {
            statistics.downloadSpeed
        }
        LOGGER.info("\n\nSUMMARY")
        LOGGER.info("=======\n")
        LOGGER.info("Items in feed:                       {}", statistics.totalFiles)
        LOGGER.info("Downloaded files:                    {}", statistics.downloadedFiles)
        LOGGER.info("Download speed:                      {}", downloadSpeed)
        LOGGER.info("Files too old:                       {}", statistics.filesTooOld)
        LOGGER.info("Existing files:                      {}", statistics.existingFilesWithMatchingSize)
        LOGGER.info("Existing files with unexpected size: {}", statistics.existingFilesWithDifferentSize)
        LOGGER.info("Failed downloads:                    {}", statistics.failedDownloads)
    }

}