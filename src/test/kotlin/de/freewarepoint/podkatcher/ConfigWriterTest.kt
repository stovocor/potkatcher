package de.freewarepoint.podkatcher

import de.freewarepoint.podkatcher.options.ParsedOptions
import org.assertj.core.api.Assertions.assertThat
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.io.TempDir
import org.springframework.boot.ApplicationArguments
import org.springframework.boot.DefaultApplicationArguments
import java.nio.file.Path

class ConfigWriterTest {

    @TempDir
    lateinit var tempDir: Path

    @Test
    fun writeDefaultConfig() {
        val configFile = tempDir.resolve("podcast-downloader.yaml")
        val args = arrayOf("--write-config", "http://localhost")
        val arguments: ApplicationArguments = DefaultApplicationArguments(*args)
        val parsedOptions = ParsedOptions(arguments)
        val configWriter = ConfigWriter(parsedOptions)
        configWriter.setConfigFile(configFile)
        configWriter.write()

        assertThat(configFile).isRegularFile
    }

}