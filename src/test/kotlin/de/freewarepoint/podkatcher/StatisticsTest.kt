package de.freewarepoint.podkatcher

import org.assertj.core.api.Assertions
import org.junit.jupiter.api.Test
import org.springframework.util.unit.DataSize

class StatisticsTest {

    @Test
    fun getDownloadSpeed() {
        val statistics = Statistics()
        statistics.recordDownloadedFile(DataSize.ofMegabytes(100).toBytes())
        statistics.recordDownloadedFile(DataSize.ofMegabytes(150).toBytes())
        statistics.setDurationInMillis(69 * 1000.toLong())
        Assertions.assertThat(statistics.downloadSpeed).isEqualTo("250MB in 69s (3MB/s)")
    }

    @Test
    fun fastDownload() {
        val statistics = Statistics()
        statistics.recordDownloadedFile(DataSize.ofMegabytes(100).toBytes())
        statistics.recordDownloadedFile(DataSize.ofMegabytes(150).toBytes())
        statistics.setDurationInMillis(300)
        Assertions.assertThat(statistics.downloadSpeed).isEqualTo("250MB in 1s (250MB/s)")
    }

}