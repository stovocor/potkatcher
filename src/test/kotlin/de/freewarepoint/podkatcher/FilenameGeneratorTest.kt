package de.freewarepoint.podkatcher

import be.ceau.podcastparser.models.core.Item
import be.ceau.podcastparser.models.support.Enclosure
import de.freewarepoint.podkatcher.options.ParsedOptions
import org.assertj.core.api.Assertions.assertThat
import org.junit.jupiter.api.Test
import org.springframework.boot.ApplicationArguments
import org.springframework.boot.DefaultApplicationArguments
import java.time.ZoneId
import java.time.ZonedDateTime

class FilenameGeneratorTest {

    @Test
    fun `filename for item`() {
        val args = arrayOf("http://localhost")
        val arguments: ApplicationArguments = DefaultApplicationArguments(*args)
        val filenameGenerator = FilenameGenerator(ParsedOptions(arguments))
        val item = Item()
        item.pubDate = ZonedDateTime.of(2018, 11, 12, 0, 0, 0, 0, ZoneId.systemDefault())
        item.setTitle("Podcast welcome")
        val enclosure = Enclosure()
        enclosure.url = "http://localhost/welcome.mp3"
        item.enclosure = enclosure
        val filename = filenameGenerator.filenameForItem(item)
        assertThat(filename).isEqualTo("2018-11-12 - Podcast welcome.mp3")
    }

    @Test
    fun `date only`() {
        val args = arrayOf("--format=%{date}", "http://localhost")
        val arguments: ApplicationArguments = DefaultApplicationArguments(*args)
        val filenameGenerator = FilenameGenerator(ParsedOptions(arguments))
        val item = Item()
        item.pubDate = ZonedDateTime.of(2018, 11, 12, 0, 0, 0, 0, ZoneId.systemDefault())
        val enclosure = Enclosure()
        enclosure.url = "http://localhost/welcome.mp3"
        item.enclosure = enclosure
        val filename = filenameGenerator.filenameForItem(item)
        assertThat(filename).isEqualTo("2018-11-12.mp3")
    }

    @Test
    fun `other date format`() {
        val args = arrayOf("--format=%{day}.%{month}.%{year}", "http://localhost")
        val arguments: ApplicationArguments = DefaultApplicationArguments(*args)
        val filenameGenerator = FilenameGenerator(ParsedOptions(arguments))
        val item = Item()
        item.pubDate = ZonedDateTime.of(2018, 5, 4, 0, 0, 0, 0, ZoneId.systemDefault())
        val enclosure = Enclosure()
        enclosure.url = "http://localhost/welcome.mp3"
        item.enclosure = enclosure
        val filename = filenameGenerator.filenameForItem(item)
        assertThat(filename).isEqualTo("04.05.2018.mp3")
    }

    @Test
    fun `different file extension`() {
        val args = arrayOf("http://localhost")
        val arguments: ApplicationArguments = DefaultApplicationArguments(*args)
        val filenameGenerator = FilenameGenerator(ParsedOptions(arguments))
        val item = Item()
        item.pubDate = ZonedDateTime.of(2018, 11, 12, 0, 0, 0, 0, ZoneId.systemDefault())
        item.setTitle("Podcast welcome")
        val enclosure = Enclosure()
        enclosure.url = "http://localhost/welcome.ogg"
        item.enclosure = enclosure
        val filename = filenameGenerator.filenameForItem(item)
        assertThat(filename).isEqualTo("2018-11-12 - Podcast welcome.ogg")
    }

    @Test
    fun `replace invalid characters`() {
        val args = arrayOf("http://localhost")
        val arguments: ApplicationArguments = DefaultApplicationArguments(*args)
        val filenameGenerator = FilenameGenerator(ParsedOptions(arguments))
        val item = Item()
        item.pubDate = ZonedDateTime.of(2018, 5, 4, 0, 0, 0, 0, ZoneId.systemDefault())
        item.setTitle("Title / containing invalid \\ characters")
        val enclosure = Enclosure()
        enclosure.url = "http://localhost/welcome.mp3"
        item.enclosure = enclosure
        val filename = filenameGenerator.filenameForItem(item)
        assertThat(filename).isEqualTo("2018-05-04 - Title _ containing invalid _ characters.mp3")
    }


}