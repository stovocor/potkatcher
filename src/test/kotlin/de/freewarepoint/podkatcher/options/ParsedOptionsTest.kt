package de.freewarepoint.podkatcher.options

import org.assertj.core.api.Assertions
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.io.TempDir
import org.springframework.boot.ApplicationArguments
import org.springframework.boot.DefaultApplicationArguments
import java.net.URL
import java.nio.file.Files
import java.nio.file.Path
import java.time.LocalDate
import java.time.ZoneId

class ParsedOptionsTest {

    @TempDir
    lateinit var tempDir: Path


    @Test
    fun `get Url`() {
        val args = arrayOf("http://localhost")
        val arguments: ApplicationArguments = DefaultApplicationArguments(*args)
        val parsedOptions = ParsedOptions(arguments)
        Assertions.assertThat(parsedOptions.getUrl()).isEqualTo(URL("http://localhost"))
    }


    @Test
    fun `get Format`() {
        val args = arrayOf("--format=%{date}", "http://localhost")
        val arguments: ApplicationArguments = DefaultApplicationArguments(*args)
        val parsedOptions = ParsedOptions(arguments)
        Assertions.assertThat(parsedOptions.getFormat()).isEqualTo("%{date}")
    }


    @Test
    fun `default format`() {
        val args = arrayOf("http://localhost")
        val arguments: ApplicationArguments = DefaultApplicationArguments(*args)
        val parsedOptions = ParsedOptions(arguments)
        Assertions.assertThat(parsedOptions.getFormat()).isEqualTo("%{date} - %{title}")
    }


    @Test
    fun `dry-run`() {
        val args = arrayOf("--dry-run", "http://localhost")
        val arguments: ApplicationArguments = DefaultApplicationArguments(*args)
        val parsedOptions = ParsedOptions(arguments)
        Assertions.assertThat(parsedOptions.isDryRun()).isTrue
    }

    @Test
    fun `default dry-run`() {
        val args = arrayOf("http://localhost")
        val arguments: ApplicationArguments = DefaultApplicationArguments(*args)
        val parsedOptions = ParsedOptions(arguments)
        Assertions.assertThat(parsedOptions.isDryRun()).isFalse
    }

    @Test
    fun `write config-file`() {
        val args = arrayOf("--write-config", "http://localhost")
        val arguments: ApplicationArguments = DefaultApplicationArguments(*args)
        val parsedOptions = ParsedOptions(arguments)
        Assertions.assertThat(parsedOptions.isWriteConfigFile()).isTrue
    }

    @Test
    fun `default write config-file`() {
        val args = arrayOf("http://localhost")
        val arguments: ApplicationArguments = DefaultApplicationArguments(*args)
        val parsedOptions = ParsedOptions(arguments)
        Assertions.assertThat(parsedOptions.isWriteConfigFile()).isFalse
    }

    @Test
    fun `output-directory`() {
        val outputDir = tempDir.resolve("output")
        Files.createDirectories(outputDir)
        val args = arrayOf("--output-directory=" + outputDir.toAbsolutePath(), "http://localhost")
        val arguments: ApplicationArguments = DefaultApplicationArguments(*args)
        val parsedOptions = ParsedOptions(arguments)
        Assertions.assertThat(parsedOptions.getOutputDirectory()).isEqualTo(outputDir)
    }

    @Test
    fun `missing output-directory`() {
        val outputDir = tempDir.resolve("output")
        val args = arrayOf("--output-directory=" + outputDir.toAbsolutePath(), "http://localhost")
        val arguments: ApplicationArguments = DefaultApplicationArguments(*args)
        try {
            ParsedOptions(arguments)
            Assertions.failBecauseExceptionWasNotThrown<Any>(ArgumentParserException::class.java)
        } catch (e: ArgumentParserException) {
            Assertions.assertThat(e).hasMessageEndingWith("does not exist.")
        }
    }

    @Test
    fun `output-directory is file`() {
        val outputDir = tempDir.resolve("output")
        Files.createFile(outputDir)
        val args = arrayOf("--output-directory=" + outputDir.toAbsolutePath(), "http://localhost")
        val arguments: ApplicationArguments = DefaultApplicationArguments(*args)
        try {
            ParsedOptions(arguments)
            Assertions.failBecauseExceptionWasNotThrown<Any>(ArgumentParserException::class.java)
        } catch (e: ArgumentParserException) {
            Assertions.assertThat(e).hasMessageEndingWith("is not a directory.")
        }
    }

    @Test
    fun `default since`() {
        val args = arrayOf("http://localhost")
        val arguments: ApplicationArguments = DefaultApplicationArguments(*args)
        val parsedOptions = ParsedOptions(arguments)
        Assertions.assertThat(parsedOptions.getSinceDate())
            .isEqualTo(LocalDate.MIN.atStartOfDay(ZoneId.systemDefault()))
    }

    @Test
    fun since() {
        val args = arrayOf("http://localhost", "--since=1991-08-25")
        val arguments: ApplicationArguments = DefaultApplicationArguments(*args)
        val parsedOptions = ParsedOptions(arguments)
        Assertions.assertThat(parsedOptions.getSinceDate())
            .isEqualTo(LocalDate.of(1991, 8, 25).atStartOfDay(ZoneId.systemDefault()))
    }

}