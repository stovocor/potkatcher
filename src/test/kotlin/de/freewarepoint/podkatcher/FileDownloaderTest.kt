package de.freewarepoint.podkatcher

import de.freewarepoint.podkatcher.options.ParsedOptions
import org.assertj.core.api.Assertions.assertThat
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.io.TempDir
import org.mockito.Mockito
import org.mockito.Mockito.mock
import java.nio.file.Files
import java.nio.file.Path

class FileDownloaderTest {

    @TempDir
    lateinit var tempDir: Path

    @Test
    fun download() {
        val fileToDownload = tempDir.resolve("source.txt")
        val targetFile = tempDir.resolve("target.txt")
        val content = "File content"
        Files.writeString(fileToDownload, content)
        val urlToDownload = fileToDownload.toUri().toURL()

        val statistics = Statistics()
        val fileDownloader = FileDownloader(mock(ParsedOptions::class.java), statistics)
        fileDownloader.download(urlToDownload, targetFile, content.length.toLong())

        assertThat(targetFile).isRegularFile
        assertThat(targetFile).hasContent(content)
        assertThat(statistics.downloadedFiles).isOne
    }

    @Test
    fun createRequiredDirectories() {
        val fileToDownload = tempDir.resolve("source.txt")
        val targetFile = tempDir.resolve("subdirectory/target.txt")
        val content = "File content"
        Files.writeString(fileToDownload, content)
        val urlToDownload = fileToDownload.toUri().toURL()

        val statistics = Statistics()
        val fileDownloader = FileDownloader(mock(ParsedOptions::class.java), statistics)
        fileDownloader.download(urlToDownload, targetFile, content.length.toLong())

        assertThat(targetFile).isRegularFile
        assertThat(targetFile).hasContent(content)
        assertThat(statistics.downloadedFiles).isOne
    }

    @Test
    fun doNothingOnDryRun() {
        val fileToDownload = tempDir.resolve("source.txt")
        val targetFile = tempDir.resolve("target.txt")
        val urlToDownload = fileToDownload.toUri().toURL()
        val parsedOptions = mock(ParsedOptions::class.java)
        Mockito.`when`(parsedOptions.isDryRun()).thenReturn(true)

        val statistics = Statistics()
        val fileDownloader = FileDownloader(parsedOptions, statistics)
        fileDownloader.download(urlToDownload, targetFile, 0)

        assertThat(targetFile).doesNotExist()
        assertThat(statistics.downloadedFiles).isOne
    }


}