package de.freewarepoint.podkatcher

import de.freewarepoint.podkatcher.options.ParsedOptions
import org.assertj.core.api.Assertions.assertThat
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.io.TempDir
import org.springframework.boot.ApplicationArguments
import org.springframework.boot.DefaultApplicationArguments
import java.nio.file.Path
import java.time.LocalDate
import java.time.ZoneId

class ConfigReaderTest {

    @TempDir
    lateinit var tempDir: Path

    @Test
    fun readDefaultConfig() {
        val configFile = tempDir.resolve("podcast-downloader.yaml")
        val args = arrayOf("--write-config", "--since=1991-08-25", "http://localhost")
        val arguments: ApplicationArguments = DefaultApplicationArguments(*args)
        val parsedOptions = ParsedOptions(arguments)
        val configWriter = ConfigWriter(parsedOptions)
        configWriter.setConfigFile(configFile)
        configWriter.write()

        val readConfig = ConfigReader.readConfigFile(configFile) ?: throw RuntimeException("Config missing")
        val emptyArgs: ApplicationArguments = DefaultApplicationArguments("http://localhost")
        val optionsFromConfig = ParsedOptions(emptyArgs, readConfig)

        assertThat(readConfig).isNotNull
        assertThat(optionsFromConfig.getSinceDate())
            .isEqualTo(LocalDate.of(1991, 8, 25).atStartOfDay(ZoneId.systemDefault()))
    }


}