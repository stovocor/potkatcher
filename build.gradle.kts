import org.jetbrains.kotlin.gradle.tasks.KotlinCompile
import org.springframework.boot.gradle.plugin.SpringBootPlugin

plugins {
	id("org.springframework.boot") version "3.2.4"
	kotlin("jvm") version "1.9.20"
	kotlin("plugin.spring") version "1.9.23"
}

group = "de.freewarepoint"
version = "1.5.1-SNAPSHOT"

repositories {
	mavenCentral()
}

dependencies {
	implementation(platform(SpringBootPlugin.BOM_COORDINATES))
	implementation("org.springframework.boot:spring-boot-starter")
	implementation("be.ceau:podcast-parser:0.11")
	implementation("org.apache.commons:commons-text:1.11.0")
	implementation("com.amihaiemil.web:eo-yaml:7.2.0")
	implementation("org.jetbrains:annotations:24.1.0")
	testImplementation("org.springframework.boot:spring-boot-starter-test")
	testImplementation("org.junit.jupiter:junit-jupiter-api")
	testRuntimeOnly("org.junit.jupiter:junit-jupiter-engine")
	testRuntimeOnly("org.junit.platform:junit-platform-launcher")
	testImplementation("org.assertj:assertj-core")
	testImplementation("org.mockito:mockito-core")
}

tasks.processResources {
	filesMatching("help.txt") {
		filter {
			it.replace("\${version}", version.toString())
		}
	}
}

tasks.withType<Test> {
	useJUnitPlatform()
}

tasks.withType<KotlinCompile> {
	kotlinOptions {
		freeCompilerArgs = listOf("-Xjsr305=strict")
		jvmTarget = "17"
	}
}

tasks.bootJar {
	launchScript()
}
